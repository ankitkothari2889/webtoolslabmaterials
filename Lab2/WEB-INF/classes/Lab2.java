import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;

public class Lab2 extends HttpServlet {
	
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException
  {
      // Set response content type
		response.setContentType("text/html");
        PrintWriter out = response.getWriter();
		
		Cookie[] cookies = request.getCookies();
		String userName = "";
		String comments = "";
		
		if(cookies!=null){        
		for (int i = 0; i < cookies.length; i++) {
            Cookie c = cookies[i];
			
            String name = c.getName();
			if(name.equals("cookieDate")){
            String value = c.getValue();
            out.println("This information was already submitted on "+value);
			}
			if(name.equals("userName")){
				userName = c.getValue();
			}
			if(name.equals("cookieComments")){
				comments = c.getValue();
			}
        }
		
		}
		
		
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Lab2</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<form action='Lab2' method='post'>");
		out.println("Name:<input type='text' name='userName' value='"+userName+"'/><br/><br />");
		out.println("Comments:<textarea rows='5' cols='20' name ='comments'>"+comments+"</textarea><br /><br />");
		out.println("Contact me: <input type='radio' name='contact' value='yes' checked> Yes <input type='radio' name='contact' value='no'> No<br> <br />");
		out.println("<input type = 'checkbox' name = 'rememberMe' value = 'yes'> Remember me for 1 week <br />");
		out.println("<input type='submit' value='SUBMIT'/>");
        out.println("</body>");
        out.println("</html>");
		
		
		
  }
  
  
  
   public void doPost(HttpServletRequest request, HttpServletResponse response){  
    try{  
  
    response.setContentType("text/html");  
    PrintWriter out = response.getWriter();
	Date date;
	
		out.println("<html>");
        out.println("<head>");
        out.println("<title>Lab2 Form Submitted</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h3>Thank you for providing your suggestions</h3>");
		out.println("Name:"+request.getParameter("userName")+"<br/>");
		out.println("Comments:"+request.getParameter("comments")+"<br/>");
		out.println("Contact me:"+request.getParameter("contact")+"<br/>");		
		
		
        out.println("</body>");
        out.println("</html>");
		
          
	if(request.getParameter("rememberMe").equals("yes")){
	date = new Date();
    Cookie cookieName=new Cookie("userName",request.getParameter("userName"));
	Cookie cookieComments = new Cookie("cookieComments",request.getParameter("comments"));
	Cookie cookieContacts = new Cookie("cookieContacts",request.getParameter("contact"));
	Cookie cookieCurrentDate = new Cookie("cookieDate",date.toString());
	//creating cookie object  
    response.addCookie(cookieName);//adding cookie in the response  
	response.addCookie(cookieComments);
	response.addCookie(cookieContacts);
	response.addCookie(cookieCurrentDate);
	
		
  
    //creating submit button  
	}
	
	out.close();  
  
        }catch(Exception e){System.out.println(e);}  
  }  
  
  
	
	
}